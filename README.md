# Call Center

## Descripcion
Solucion al problema brindado en el ejercicio, incluyendo los puntos extras.
El mismo consistia en poder atender hasta 10 llamadas de forma concurrente, teniendo 
en cuenta la prioridad de los Empleados, siendo que en la cadena de prioridades, el
primer en responder debe ser el Operador, luego el Supervisor y finalmente el Director.

### Dependencias del Proyecto
* Java 8
* Maven
* Junit
* Slf4j





 