package ar.com.almundo.callcenter.exception;

/**
 * Esta excepcion es alcanzada cuando el Call Center no tiene empleados.
 *
 * @author vfuentes
 */
public class NoEmployeesInCallCenterException extends Exception {

    public NoEmployeesInCallCenterException(String arg0) {
        super(arg0);
    }


}
