package ar.com.almundo.callcenter.exception;

/**
 * Esta excepcion es alcanzada cuando el Call Center no posee llamadas.
 *
 * @author vfuentes
 */

public class NoCallsToAnswerException extends Exception {

    public NoCallsToAnswerException(String arg0) {
        super(arg0);
    }
}
