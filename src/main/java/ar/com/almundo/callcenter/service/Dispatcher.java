package ar.com.almundo.callcenter.service;

import ar.com.almundo.callcenter.Configuration;
import ar.com.almundo.callcenter.exception.NoCallsToAnswerException;
import ar.com.almundo.callcenter.exception.NoEmployeesInCallCenterException;
import ar.com.almundo.callcenter.model.Employee;
import ar.com.almundo.callcenter.model.TelephoneCall;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Clase que se encarga de recibir derivar las llamadas a los distintos empleados segun la prioridad de cada uno.
 *
 * @author vfuentes
 */
public class Dispatcher {

    private static final Logger logger = LoggerFactory.getLogger(Dispatcher.class);
    private Configuration configuration = new Configuration();
    private PriorityQueue<Employee> employees = new PriorityQueue<>();

    /**
     * Metodo para obtener los empleados del Call Center
     *
     * @return a PriorityQueue de Employees
     */
    public synchronized PriorityQueue<Employee> getEmployees() {
        return this.employees;
    }

    /**
     * Metodo que se encarga de distribuir las llamadas para ser atendidas por el empleado que le corresponde.
     * Utilizo un FixedThreadPool que es el encargado de manejar la cantidad de llamadas concurrentes que soporta la clase Dispatcher.
     * El numero de llamadas concurrentes que es soportada, se encuentra ubicado en el archivo de configuracion config.properties.
     *
     * @param telephoneCalls lista de las llamadas de telefono a distribuir entre los empleados del Call Center.
     * @return una lista de las llamadas de Telefono que fueron contestadas
     */
    public ArrayList<TelephoneCall> dispatchCall(ArrayList<TelephoneCall> telephoneCalls) throws NoEmployeesInCallCenterException, NoCallsToAnswerException {
        ArrayList<TelephoneCall> telephoneCallsOnHold = new ArrayList<>();
        ArrayList<TelephoneCall> telephoneCallsAnswered = new ArrayList<>();


        if (this.employees.isEmpty()) {
            throw new NoEmployeesInCallCenterException("No hay ningun empleado para atender su llamada... Call Center cerrado");
        }

        if (telephoneCalls.isEmpty()) {
            throw new NoCallsToAnswerException("No hay llamadas para ser atendidas");
        }

        ExecutorService executor = Executors.newFixedThreadPool(this.configuration.getMaxConcurrentCalls());
        telephoneCalls.forEach((telephoneCall) -> assignEmployeeToTelephoneCall(telephoneCallsOnHold, telephoneCallsAnswered, executor, telephoneCall));

        executor.shutdown();

        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }

        if (!telephoneCallsOnHold.isEmpty())
            telephoneCallsAnswered.addAll(this.dispatchCall(telephoneCallsOnHold));


        return telephoneCallsAnswered;

    }

    /**
     * Metodo encargado de asignar un empleado segun su prioridad a la telephoneCall y atenderla.
     * Si no se encuentran empleados disponibles, la telephoneCall es agregada al ArrayList telephoneCallsOnHold
     *
     * @param telephoneCallsOnHold   ArrayList de las telephoneCalls que no pudieron ser contestadas por los empleados
     * @param telephoneCallsAnswered ArrayList con las telephoneCalls contestadas por los empleados
     * @param executor               instancia de la clase Executor Service
     * @param telephoneCall          llamada de telefono a contestar
     */
    private void assignEmployeeToTelephoneCall(ArrayList<TelephoneCall> telephoneCallsOnHold, ArrayList<TelephoneCall> telephoneCallsAnswered, ExecutorService executor, TelephoneCall telephoneCall) {
        Employee employee = this.employees.poll();

        if (employee != null) {
            logger.info("La llamada con Id: {}, sera atendida por el {} llamado: {}", telephoneCall.getId(), employee.getRole(), employee.getName());
            executor.submit(() -> {
                telephoneCallsAnswered.add(employee.answerTelephoneCall(telephoneCall, employee, this));

            });
        } else {
            logger.info("Todas las lineas se encuentran ocupadas. Por favor no cuelgue, la llamada con id: {} sera puesta en espera... Musica sonando", telephoneCall.getId());
            telephoneCallsOnHold.add(telephoneCall);

        }
    }


}

