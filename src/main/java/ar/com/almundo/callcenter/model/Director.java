package ar.com.almundo.callcenter.model;

/**
 * Clase que hereda la clase Employee y modela al employee Director.
 *
 * @author vfuentes
 */
public class Director extends Employee implements Comparable<Employee> {

    public Director(String name, Call call) {
        super(name, call);
        this.setPriority(3);
        this.setRole(Employee.DIRECTOR);
    }
}
