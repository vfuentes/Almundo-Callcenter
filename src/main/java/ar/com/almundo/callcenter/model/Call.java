package ar.com.almundo.callcenter.model;

import ar.com.almundo.callcenter.service.Dispatcher;

/**
 * El usuario de esta interfaz tendra el control preciso sobre la accion de contestar una llamada.
 * La interfaz provee un metodo el cual contesta una llamada de telefono, retornando la llamada contestada.
 *
 * @author vfuentes
 */
public interface Call {

    /**
     * Metodo que contesta una llamada de telefono.
     *
     * @param telephoneCall llamada de telefono que sera contestada
     * @param employee      empleado que contestara la llamada de telefono
     * @param dispatcher    central telefonica que gestiona el manejo de las llamadas de telefono
     * @return llamada de telefono que fue contestada
     */
    TelephoneCall answerTelephoneCall(TelephoneCall telephoneCall, Employee employee, Dispatcher dispatcher);

}
