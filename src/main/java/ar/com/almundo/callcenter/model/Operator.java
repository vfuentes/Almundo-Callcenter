package ar.com.almundo.callcenter.model;

/**
 * Clase que hereda la implementacion de Empleado y modela al employee Operator.
 *
 * @author vfuentes
 */
public class Operator extends Employee implements Comparable<Employee> {

    public Operator(String name, Call call) {
        super(name, call);
        this.setPriority(1);
        this.setRole(Employee.OPERATOR);
    }
}
