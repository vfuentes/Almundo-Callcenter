package ar.com.almundo.callcenter.model;


/**
 * Clase que modela una TelephoneCall
 *
 * @author vfuentes
 */
public class TelephoneCall {

    private Integer id;
    private Employee employee;
    private Integer duration;

    public TelephoneCall(Integer id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployeeThatAnswered(Employee employee) {
        this.employee = employee;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getId() {
        return id;
    }

}
