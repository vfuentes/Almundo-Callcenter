package ar.com.almundo.callcenter.model;


/**
 * Clase que hereda la implementacion de Empleado y modela al employee Supervisor.
 *
 * @author vfuentes
 */
public class Supervisor extends Employee implements Comparable<Employee> {

    public Supervisor(String name, Call call) {
        super(name, call);
        this.setPriority(2);
        this.setRole(Employee.SUPERVISOR);
    }
}
