package ar.com.almundo.callcenter.model;

import ar.com.almundo.callcenter.service.Dispatcher;

/**
 * Clase que modela a un empleado.
 *
 * @author vfuentes
 */
public abstract class Employee {

    public static final String OPERATOR = "Operator";
    public static final String SUPERVISOR = "Supervisor";
    public static final String DIRECTOR = "Director";
    private Integer priority;
    private String name;
    private Call call;
    private String role;


    Employee(String name, Call call) {
        this.name = name;
        this.call = call;
    }

    private Integer getPriority() {
        return priority;
    }

    void setPriority(int priority) {
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public String getRole() {
        return role;
    }

    void setRole(String role) {
        this.role = role;
    }

    /**
     * Metodo que compara entre los empleados y ordena segun su prioridad
     *
     * @param employee empleado
     * @return resultado de la comparacion entre los empleados segun su prioridad
     */
    public int compareTo(Employee employee) {
        return this.getPriority() - employee.getPriority();
    }

    /**
     * @param telephoneCall llamada por contestar
     * @param employee      empleado que contestara la llamada segun su prioridad
     * @param dispatcher    instancia de la clase Dispatcher
     * @return llamada contestada
     */
    public TelephoneCall answerTelephoneCall(TelephoneCall telephoneCall, Employee employee, Dispatcher dispatcher) {
        return this.call.answerTelephoneCall(telephoneCall, employee, dispatcher);

    }
}

