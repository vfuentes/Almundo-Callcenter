package ar.com.almundo.callcenter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * Clase donde se encuentran las configuraciones para el correcto funcionamiento del programa.
 * Estas configuraciones se encuentran en config.properties.
 *
 * @author vfuentes
 */
public class Configuration {
    private static final Logger logger = LoggerFactory.getLogger(Configuration.class);

    private static final String CONFIG_FILE = "/config.properties";
    private static final String MAX_CONCURRENT_CALLS = "max-concurrent-calls";


    private final Integer maxConcurrentCalls;


    public Configuration() {
        super();
        Properties properties = this.loadConfig(CONFIG_FILE);

        this.maxConcurrentCalls = Integer.parseInt(properties.getProperty(MAX_CONCURRENT_CALLS));

    }


    public Integer getMaxConcurrentCalls() {
        return maxConcurrentCalls;
    }


    /**
     * Este metodo carga el archivo de configuracion para que puedan ser leidos los parametros.
     *
     * @return object properties.
     */
    private Properties loadConfig(String propertiesFileName) {
        Properties configFile = new Properties();
        try {
            configFile.load(getClass().getResourceAsStream(propertiesFileName));
        } catch (IOException e) {
            logger.error("Problema cargando el archivo de configuracion.", e);
        }
        return configFile;
    }


}