package ar.com.almundo.callcenter.service;

import ar.com.almundo.callcenter.SimulateCall;
import ar.com.almundo.callcenter.exception.NoCallsToAnswerException;
import ar.com.almundo.callcenter.exception.NoEmployeesInCallCenterException;
import ar.com.almundo.callcenter.model.*;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Clase que testea la clase Dispatcher
 *
 * @author vfuentes
 */
public class DispatcherTest {

    private static final Logger logger = LoggerFactory.getLogger(DispatcherTest.class);

    private final Dispatcher dispatcher = new Dispatcher();


    /**
     * Metodo que crea 4 empleados para el Call Center.
     *
     * @param dispatcher instancia de la Clase Dispatcher
     */
    private void generateFourEmployees(Dispatcher dispatcher) {

        SimulateCall simulateCall = new SimulateCall();

        dispatcher.getEmployees().add(new Supervisor("Agustin", simulateCall));
        dispatcher.getEmployees().add(new Operator("Juan", simulateCall));
        dispatcher.getEmployees().add(new Director("Matias", simulateCall));
        dispatcher.getEmployees().add(new Operator("Tobias", simulateCall));

    }

    /**
     * Metodo que crea 10 empleados para el Call Center.
     *
     * @param dispatcher instancia de la Clase Dispatcher
     */
    private void generateTenEmployees(Dispatcher dispatcher) {

        SimulateCall simulateCall = new SimulateCall();

        dispatcher.getEmployees().add(new Supervisor("Agustin", simulateCall));
        dispatcher.getEmployees().add(new Supervisor("Martin", simulateCall));
        dispatcher.getEmployees().add(new Operator("Juan", simulateCall));
        dispatcher.getEmployees().add(new Director("Matias", simulateCall));
        dispatcher.getEmployees().add(new Operator("Santiago", simulateCall));
        dispatcher.getEmployees().add(new Operator("Manuel", simulateCall));
        dispatcher.getEmployees().add(new Operator("Tomas", simulateCall));
        dispatcher.getEmployees().add(new Operator("Victor", simulateCall));
        dispatcher.getEmployees().add(new Operator("Pablo", simulateCall));
        dispatcher.getEmployees().add(new Operator("Tobias", simulateCall));

    }


    /**
     * Metodo que crea las llamadas que seran atendidas en el Call Center.
     *
     * @param num cantidad de llamadas a crear.
     * @return lista de llamadas creadas.
     */
    private ArrayList<TelephoneCall> generateCalls(int num) {
        ArrayList<TelephoneCall> telephoneCalls = new ArrayList<>();
        for (int i = 1; i < num + 1; i++) {
            TelephoneCall call = new TelephoneCall(i);
            telephoneCalls.add(call);
            logger.info("Se ha creado la llamada con id: " + call.getId());
        }
        return telephoneCalls;
    }


    /**
     * Metodo que prueba la correcta creacions de 4 empleados del Call Center.
     */
    @Test
    public void testGenerateFourEmployees() {

        generateFourEmployees(dispatcher);
        assertEquals(4, dispatcher.getEmployees().size());
    }

    /**
     * Metodo que prueba la correcta creacions de 10 empleados del Call Center.
     */
    @Test
    public void testGenerateTenEmployees() {

        generateTenEmployees(dispatcher);
        assertEquals(10, dispatcher.getEmployees().size());
    }

    /**
     * Metodo que prueba el correcto ordenamiento de 4 empleados segun la prioridad de los mismos. Primero deben estar los Operator,
     * luego los Supervisor y finalmente los Director.
     */
    @Test
    public void testPriorityQueueFourEmployees() {

        generateFourEmployees(dispatcher);
        assertEquals(Employee.OPERATOR, dispatcher.getEmployees().poll().getRole());
        assertEquals(Employee.OPERATOR, dispatcher.getEmployees().poll().getRole());
        assertEquals(Employee.SUPERVISOR, dispatcher.getEmployees().poll().getRole());
        assertEquals(Employee.DIRECTOR, dispatcher.getEmployees().poll().getRole());

    }

    /**
     * Metodo que prueba el correcto ordenamiento de 10 empleados segun la prioridad de los mismos. Primero deben estar los Operator,
     * luego los Supervisor y finalmente los Director.
     */
    @Test
    public void testPriorityQueueTenEmployees() {

        generateTenEmployees(dispatcher);
        assertEquals(Employee.OPERATOR, dispatcher.getEmployees().poll().getRole());
        assertEquals(Employee.OPERATOR, dispatcher.getEmployees().poll().getRole());
        assertEquals(Employee.OPERATOR, dispatcher.getEmployees().poll().getRole());
        assertEquals(Employee.OPERATOR, dispatcher.getEmployees().poll().getRole());
        assertEquals(Employee.OPERATOR, dispatcher.getEmployees().poll().getRole());
        assertEquals(Employee.OPERATOR, dispatcher.getEmployees().poll().getRole());
        assertEquals(Employee.OPERATOR, dispatcher.getEmployees().poll().getRole());
        assertEquals(Employee.SUPERVISOR, dispatcher.getEmployees().poll().getRole());
        assertEquals(Employee.SUPERVISOR, dispatcher.getEmployees().poll().getRole());
        assertEquals(Employee.DIRECTOR, dispatcher.getEmployees().poll().getRole());

    }

    /**
     * Metodo que prueba la correcta creacion de las llamadas de telefono.
     */
    @Test
    public void testGenerateCalls() {

        ArrayList<TelephoneCall> telephoneCalls = generateCalls(10);
        assertEquals(10, telephoneCalls.size());

    }


    /**
     * Metodo que prueba que sucede cuando el metodo dispatchCall() no recibe una llamada.
     */
    @Test(expected = NoCallsToAnswerException.class)
    public void testDispatchZeroCallWithFourEmployees() throws NoEmployeesInCallCenterException, NoCallsToAnswerException {

        generateFourEmployees(dispatcher);
        dispatcher.dispatchCall(generateCalls(0));

    }


    /**
     * Metodo que prueba que sucede cuando el metodo dispatchCall() recibe una llamada con 0 empleados.
     */
    @Test(expected = NoEmployeesInCallCenterException.class)
    public void testDispatchOneCallWithNoEmployees() throws NoEmployeesInCallCenterException, NoCallsToAnswerException {

        dispatcher.dispatchCall(generateCalls(1));

    }


    /**
     * Metodo que prueba que sucede cuando el metodo dispatchCall() recibe una llamada con 4 empleados.
     */
    @Test
    public void testDispatchOneCallWithFourEmployees() throws NoEmployeesInCallCenterException, NoCallsToAnswerException {

        generateFourEmployees(dispatcher);
        ArrayList<TelephoneCall> answeredCalls = dispatcher.dispatchCall(generateCalls(1));
        assertEquals(4, dispatcher.getEmployees().size());
        assertEquals(1, answeredCalls.size());

    }

    /**
     * Metodo que prueba que sucede cuando el metodo dispatchCall() recibe una llamada con 10 empleados.
     */
    @Test
    public void testDispatchOneCallWithTenEmployees() throws NoEmployeesInCallCenterException, NoCallsToAnswerException {

        generateTenEmployees(dispatcher);
        ArrayList<TelephoneCall> answeredCalls = dispatcher.dispatchCall(generateCalls(1));
        assertEquals(10, dispatcher.getEmployees().size());
        assertEquals(1, answeredCalls.size());

    }

    /**
     * Metodo que prueba que sucede cuando el metodo dispatchCall() recibe 10 llamadas y son atendidas por 4 empleados.
     */
    @Test
    public void testDispatchTenCallsWithFourEmployees() throws NoEmployeesInCallCenterException, NoCallsToAnswerException {

        generateFourEmployees(dispatcher);
        ArrayList<TelephoneCall> answeredCalls = dispatcher.dispatchCall(generateCalls(10));
        assertEquals(4, dispatcher.getEmployees().size());
        assertEquals(10, answeredCalls.size());

    }

    /**
     * Metodo que prueba que sucede cuando el metodo dispatchCall() recibe 10 llamadas y son atendidas por 10 empleados.
     */
    @Test
    public void testDispatchTenCallsWithTenEmployees() throws NoEmployeesInCallCenterException, NoCallsToAnswerException {

        generateTenEmployees(dispatcher);
        ArrayList<TelephoneCall> answeredCalls = dispatcher.dispatchCall(generateCalls(10));
        assertEquals(10, dispatcher.getEmployees().size());
        assertEquals(10, answeredCalls.size());

    }

    /**
     * Metodo que prueba que sucede cuando el metodo dispatchCall() recibe 12 llamadas y son atendidas por 4 empleados.
     */
    @Test
    public void testDispatchTwelveCallsWithFourEmployees() throws NoEmployeesInCallCenterException, NoCallsToAnswerException {

        generateFourEmployees(dispatcher);
        ArrayList<TelephoneCall> answeredCalls = dispatcher.dispatchCall(generateCalls(12));
        assertEquals(4, dispatcher.getEmployees().size());
        assertEquals(12, answeredCalls.size());

    }

    /**
     * Metodo que prueba que sucede cuando el metodo dispatchCall() recibe 12 llamadas y son atendidas por 10 empleados.
     */
    @Test
    public void testDispatchTwelveCallsWithTenEmployees() throws NoEmployeesInCallCenterException, NoCallsToAnswerException {

        generateTenEmployees(dispatcher);
        ArrayList<TelephoneCall> answeredCalls = dispatcher.dispatchCall(generateCalls(12));
        assertEquals(10, dispatcher.getEmployees().size());
        assertEquals(12, answeredCalls.size());

    }

}
