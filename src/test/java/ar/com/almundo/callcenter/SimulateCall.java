package ar.com.almundo.callcenter;

import ar.com.almundo.callcenter.model.Call;
import ar.com.almundo.callcenter.model.Employee;
import ar.com.almundo.callcenter.model.TelephoneCall;
import ar.com.almundo.callcenter.service.Dispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

/**
 * Clase utilizada para simular una llamada de telefono y su duracion.
 *
 * @author vfuentes
 */

public class SimulateCall implements Runnable, Call {

    private static final Logger logger = LoggerFactory.getLogger(SimulateCall.class);
    private Integer duration;


    /**
     * Metodo que simula la llamada
     */
    public void run() {
        try {
            this.duration = getDuration();
            Thread.sleep(TimeUnit.SECONDS.toMillis(this.duration));

        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }


    }

    /**
     * Metodo que calcula la duracion de la llamada.
     * Origin es el numero base para calcular la duracion de la llamada.
     * Bound es el numero maximo para calcular la duracion de la llamada(Se debe sumar 1)
     * Por ejemplo si la duracion maxima deseada para la llamada es 10, en el campo bound, se debe ingresar 11.
     *
     * @return duracion de la llamada
     */
    private Integer getDuration() {

        return ThreadLocalRandom.current().nextInt(5, 11);

    }

    @Override
    public TelephoneCall answerTelephoneCall(TelephoneCall telephoneCall, Employee employee, Dispatcher dispatcher) {

        logger.info("La llamada con Id: {}, esta siendo atendida por el {} llamado: {}", telephoneCall.getId(), employee.getRole(), employee.getName());
        run();
        telephoneCall.setDuration(this.duration);
        telephoneCall.setEmployeeThatAnswered(employee);
        finishCall(telephoneCall);
        dispatcher.getEmployees().add(employee);
        return telephoneCall;
    }

    /**
     * Metodo que indica la finalizacion de la llamada
     *
     * @param telephoneCall llamada de telefono
     */
    private void finishCall(TelephoneCall telephoneCall) {

        logger.info("La llamada con Id: {}, fue atendida por el {} llamado: {} cuya duracion fue de: {} segundos", telephoneCall.getId(), telephoneCall.getEmployee().getRole(), telephoneCall.getEmployee().getName(), telephoneCall.getDuration());

    }
}
